#! /usr/bin/perl
# This file is part of acmeman.
# Copyright (C) 2014, 2017 Sergey Poznyakoff <gray@gnu.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use CPAN;
use Cwd qw(getcwd);
use File::Path qw(mkpath);

####
my $incdir = "inc/ExtUtils";
my $topdir = getcwd;

my $modname = "ExtUtils::AutoInstall";

my $mod = CPAN::Shell->expandany($modname) or die "Can't expand $modname";
my $file = $mod->inst_file;
my $dir;

if (defined($file)) {
    print "$modname installed at $file\n";
    if (-f $file) {
	$file =~ s#/[^/]+\.pm$##;
	if (-d $file) {
	    $dir = $file;
	} else {
	    die "Can't find $modname directory";
	}
    }
} else {
    print "Getting $modname\n";
    my $distro = $mod->get or die "Can't get distribution for $modname";
    $dir = $distro->dir;
}

$file = "$dir/$incdir/AutoInstall.pm";
$file = "$dir/AutoInstall.pm" unless (-f $file);
-f $file or die "$file not found";

use autodie;
chdir $topdir;
mkpath $incdir unless -d $incdir;
chdir $incdir;
no autodie;
unlink "AutoInstall.pm";
use autodie;
symlink $file, "AutoInstall.pm";

